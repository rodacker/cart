# Simple PHP cart library

A simple PHP cart library offering several interfaces to support easy adaption to your needs. 


You can find a basic example implementation at:   

https://gitlab.com/rodacker/cart-example

### Important

**this library is under active development and not recommenden for use in production environments**

## Install

It is recommended to install this package via [Composer](https://getcomposer.org/):

```
composer require rodacker/cart
```

## Usage

### Creating the cart

Create the cart object with `new Cart()` passing an identifier object implementing `CartIdentifierInterface`,  
an storage object implementing `CartStorageInterface` and a `Currency` object from the `moneyphp/money` library:  

```php
$cart = new Cart(new Session(), new SessionStorage(), new Currency('EUR');
```

#### Identifier and Storage
 This package ships with two basic identifier classes:
 
  * `Rodacker\Cart\Identifier\Cookie`
  * `Rodacker\Cart\Identifier\Session`

   and with one `SessionStorage` class: 
   
   `Rodacker\Cart\Storage\SessionStorage`:

Both identifer classes are based on native PHP session and cookie functions. This can be useful for
standalone PHP applications, but may post a problem for frameworks like Symfony using their own session 
 implementation. In the latter case you have to create your own identifier class implementing
 `CartIdentifierInterface`
 
The same with the `SessionStorage` class. It uses native PHP session functions to store the cart. 
Replace it with an own class implementing `CartSessionInterface` and you will be fine.

### Interacting with cart items

The cart provides several function to interact with the items stored in the cart. 

#### Cart items

The package comes with a `CartItem` class. This object does not implement any interface yet, 
but will in the future. 

Create a new item with:

```php
$item = new CartItem($product, Money::EUR($price), $quantity);
```

 * `$product` needs to be a class implementing `CartItemInterface`
 * `$price` is the `Money` object representing the price as smallest possible value 
 in the given currency, e.g. *100 = 1 €* or *900 = 9.99 €* Please read the [Money documenation](http://moneyphp.org/en/latest/getting-started.html) how to work with the `Money object
 * `$quantity` is optional (defaults to `1`) and needs to be an `integer > 1

#### Product/Article class
As of now there is no Product or Article class shipping with this package, as I thought
in most cases you will make your existing product classes usable by implementing the 
`CartItemInterface`. I will add a simple class in the near future.

You can find a basic `Arcticle` class in the [cart example](https://gitlab.com/rodacker/cart-example/blob/0.1.0/src/Article.php)

```php
 $article = new Article($name, $price, $images);
```

 * `$name` is a string
 * `$price` is the `Money` object representing the price as smallest possible value 
  in the given currency, e.g. *100 = 1 €* or *900 = 9.99 €* Please read the [Money documenation](http://moneyphp.org/en/latest/getting-started.html) how to work with the `Money object
 * `$images` an array if objects implementing `CartImageInterface`. 
 The related functions can return an empty array if you do not need images in the cart 

#### Add an item
Pass a `CartItem` object to the function. If the cart includes an item with the same product, 
the quantiy will be added to the existing one.

```php
/** @var CartItemInterface $item */
$cart->addItem($item);
```

#### remove an item**
Pass a `CartItem` object to the function.

```php
/** @var CartItemInterface $item */
$cart->removeItem($item);
```

#### Update item quantity
Pass the `CartItem` object, the quantity difference and the decrease flag (defaults to `false`)  to
the function. The minimum quantity is limited to `1`  

```php
$cart->updateItemQuantity($item, $diff, $decrease = false);
```

**... to be continued**

## Todo
Some refactoring ideas and features for the future: 

 * add attributes support
 * add price manipulation rules (e.g. coupons, per item, per cart, etc.)
 * add shipping costs support (not sure about this) 
 * add tax support
 * add more constraints and exceptions  
 * write more tests for constraints, exceptsion and edge cases 
 * create new identifier and storage implementations based on the `symfony/http-foundation` package
  which can be used optionally.
 * evaluate event dispatcher integration
  

## Support

Please visit the [Gitlab project page](https://gitlab.com/rodacker/cart) and 
use the [issue tracker](https://gitlab.com/rodacker/cart/issues) for any feedback
or issues you may have. 


## License

This project is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

