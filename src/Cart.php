<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 06.06.17
 * Time: 20:48
 */

namespace Rodacker\Cart;

use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Exception\UnknownCurrencyException;
use Money\Money;
use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Rodacker\Cart\Item\CartItemInterface;
use Rodacker\Cart\Product\CartProductInterface;
use Rodacker\Cart\Storage\CartStorageInterface;
use Rodacker\Cart\Exception\CanNotLoadCartException;
use Rodacker\Cart\Exception\CartItemNotFoundException;

/**
 * Class Cart
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package Rodacker\Cart
 */
class Cart implements CartInterface
{

    const INCREASE_QUANTITY = 'increase';
    const DECREASE_QUANTITY = 'decrease';

    /** @var string */
    private $id;

    /** @var  CartIdentifierInterface */
    private $identifier;

    /** @var  CartStorageInterface */
    private $storage;

    /** @var  Currency */
    private $currency;

    /** @var CartItemInterface[] */
    private $items = [];

    /**
     * Cart constructor.
     *
     * @param CartIdentifierInterface $identifier
     * @param CartStorageInterface    $storage
     * @param Currency                $currency
     *
     * @throws UnknownCurrencyException
     */
    public function __construct(
        CartIdentifierInterface $identifier,
        CartStorageInterface $storage,
        Currency $currency = null
    ) {
        $this->identifier = $identifier;
        $this->storage    = $storage;

        if ($currency && !$currency->isAvailableWithin(new ISOCurrencies())) {
            throw new UnknownCurrencyException();
        } else {
            $currency = new Currency('EUR');
        }

        $this->currency   = $currency;

        // set the internal id
        $this->id = $identifier->get();

        // restore cart if available
        $this->restore();
    }

    /**
     * @return CartItemInterface[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param CartItemInterface $item
     */
    public function addItem(CartItemInterface $item)
    {

        if ($updateItem = $this->hasItemForSameProduct($item)) {
            $this->updateItemQuantity($updateItem, $item->getQuantity());
        } else {
            $this->items[$item->getId()] = $item;
        }

        $this->save();
    }

    /**
     * @param CartItemInterface $item
     *
     * @return bool|CartItemInterface
     */
    private function hasItemForSameProduct(CartItemInterface $item)
    {

        $updateItem = false;
        foreach ($this->items as $existingItem) {
            if ($existingItem->getProductId() === $item->getProductId()) {
                $updateItem = $existingItem;
            }
        }

        return $updateItem;
    }

    /**
     * @param CartItemInterface $item
     * @param          $diff
     * @param bool     $decrease
     *
     * @return int
     */
    public function updateItemQuantity(
        CartItemInterface $item,
        $diff,
        $decrease = false
    ) {
        if ($decrease) {
            return $item->decreaseQuantity($diff);
        } else {
            return $item->increaseQuantity($diff);
        }
    }

    /**
     * @param int $id
     *
     * @return CartItemInterface
     * @throws CartItemNotFoundException
     */
    public function getItem($id)
    {
        if ( !array_key_exists($id, $this->items)) {
            throw new CartItemNotFoundException(
                sprintf('the cart item with id %s does not exist', $id)
            );
        }

        return $this->items[$id];
    }

    /**
     * Remove item
     *
     * @param CartItemInterface $item
     */
    public function removeItem(CartItemInterface $item)
    {
        if ($key = array_search($item, $this->items)) {
            unset($this->items[$key]);

            $this->save();
        }
    }

    /**
     * calculcate the total sum of the cart
     *
     * @return Money
     */
    public function getTotal()
    {
        $total = new Money(0, $this->currency);

        /** @var CartItemInterface $item */
        foreach ($this->items as $item) {

            // Money objects are immutable, so each operation returns a new object
            $total = $total->add($item->getPrice()->multiply($item->getQuantity()));
        }

        return $total;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return 0 === count($this->getItems());
    }

    /**
     * @return int
     */
    public function getItemCount($unique = false)
    {

        if ( !$unique) {
            $count = 0;

            /** @var CartProductInterface $item */
            foreach ($this->items as $item) {
                $count += $item->getQuantity();
            }
        } else {
            $count = count($this->items);
        }

        return $count;
    }

    /**
     * clears the internal items array
     */
    public function clear()
    {
        $this->items = [];
        $this->save();
    }

    /**
     * save the cart in the storage
     */
    public function save()
    {
        $this->storage->save($this);
    }

    /**
     *
     */
    public function restore()
    {
        try {
            $cart = $this->storage->load($this->identifier);

            $this->items = $cart->getItems();
        } catch (CanNotLoadCartException $exception) {
            // todo: error handling if we can not load the cart
        }
    }

}
