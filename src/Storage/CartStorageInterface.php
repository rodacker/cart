<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 12.06.17
 * Time: 09:34
 */

namespace Rodacker\Cart\Storage;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Rodacker\CartBundle\Exception\CanNotLoadCartException;

/**
 * Interface CartStorageInterface
 *
 * @package Rodacker\Cart
 */
interface CartStorageInterface
{

    /**
     * @param CartIdentifierInterface $identifier
     *
     * @throws CanNotLoadCartException
     *
     * @return CartInterface
     */
    public function load(CartIdentifierInterface $identifier);

    /**
     * @param CartInterface $cart
     *
     * @return CartInterface
     */
    public function save(CartInterface $cart);

    /**
     * @param CartIdentifierInterface $identifier
     *
     * @return bool
     */
    public function delete(CartIdentifierInterface $identifier);

    /**
     * @param CartIdentifierInterface $identifier
     *
     * @return bool
     */
    public function has(CartIdentifierInterface $identifier);

}