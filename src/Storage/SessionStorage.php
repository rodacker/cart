<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 19.06.17
 * Time: 14:25
 */

namespace Rodacker\Cart\Storage;

use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Exception\CanNotLoadCartException;
use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Rodacker\Cart\Utils\Helper;

class SessionStorage implements CartStorageInterface
{

    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * @param CartIdentifierInterface $identifier
     *
     * @throws CanNotLoadCartException
     *
     * @return CartInterface
     */
    public function load(CartIdentifierInterface $identifier)
    {
        if ( !$this->has($identifier)) {
            throw new CanNotLoadCartException(
                sprintf(
                    'Can not load cart from session. No entry for storage key %s found!',
                    Helper::STORAGE_KEY
                )
            );
        }

        return unserialize($_SESSION[Helper::STORAGE_KEY]);
    }

    /**
     * @param CartInterface $cart
     *
     * @return CartInterface
     */
    public function save(CartInterface $cart)
    {
        $_SESSION[Helper::STORAGE_KEY] = serialize($cart);

        return $cart;
    }

    /**
     * @param CartIdentifierInterface $identifier
     */
    public function delete(CartIdentifierInterface $identifier)
    {
        if ($this->has($identifier)) {
            unset($_SESSION[Helper::STORAGE_KEY]);
        };
    }

    /**
     * @return bool
     */
    public function has(CartIdentifierInterface $identifier)
    {
        return array_key_exists(Helper::STORAGE_KEY, $_SESSION);
    }
}