<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 13:22
 */

namespace Rodacker\Cart\Item;

use Money\Money;
use Rodacker\Cart\Image\CartImageInterface;
use Rodacker\Cart\Product\CartProductInterface;

/**
 * Class CartItem
 *
 * @author  Patrick Rodacker <patrick.rodacker@gmail.com>
 * @package Rodacker\Cart
 *
 */
class CartItem implements CartItemInterface
{

    /** @var string */
    private $id;

    /** @var  CartProductInterface */
    private $product;

    /** @var  int */
    private $quantity;

    /** @var  Money */
    private $price;

    /** @var array */
    private $attributes = [];

    /**
     * CartItem constructor.
     *
     * @param CartProductInterface $product
     * @param Money|int|string     $price
     * @param int                  $quantity
     * @param array                $attributes
     */
    public function __construct(
        CartProductInterface $product,
        Money $price,
        $quantity = 1,
        $attributes = []
    ) {

        $this->id = $this->getUniqueId();

        $this->product  = $product;
        $this->price    = $price;
        $this->quantity = $quantity;

        foreach ($attributes as $key => $value) {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * @return string
     */
    private function getUniqueId()
    {
        return uniqid('item-');
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * return the name of the product use in the card as title
     */
    public function getName()
    {
        return $this->product->getName();
    }

    /**
     * @return int|string
     */
    public function getProductId() {
        return $this->product->getId();
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $diff
     *
     * @return int
     */
    public function increaseQuantity( int $diff) {

        return $this->quantity += $diff;
    }

    /**
     * @param int $diff
     *
     * @return int
     */
    public function decreaseQuantity(int $diff) {

        if ($diff >= $this->quantity) {
            return $this->quantity = 1;
        } else {
            return $this->quantity -= $diff;
        }
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return Money
     */
    public function getSum()
    {
        return $this->price->multiply($this->quantity);
    }

    /**
     * @return CartImageInterface[]
     */
    public function getImages()
    {
        return $this->product->getImages();
    }

    /**
     * @return bool
     */
    public function hasImages()
    {
        return 0 < count($this->getImages());
    }

    /**
     * @return null|CartImageInterface
     */
    public function getFirstImage()
    {
        $images = $this->getImages();

        return $this->hasImages() ? $images[0] : null;
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * getter for a single attributes
     *
     * @param  string $attribute
     *
     * @return mixed|null
     */
    public function getAttribute($attribute)
    {
        return array_key_exists($attribute, $this->attributes)
            ? $this->attributes[$attribute]
            : null;
    }

}