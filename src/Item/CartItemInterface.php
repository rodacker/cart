<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Item;

use Money\Money;
use Rodacker\Cart\Image\CartImageInterface;

interface CartItemInterface
{

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getProductId();

    /**
     * @return int
     */
    public function getQuantity();

    /**
     * @param int $difference
     *
     * @return int
     */
    public function increaseQuantity(int $difference);

    /**
     * @param int $difference
     *
     * @return int
     */
    public function decreaseQuantity(int $difference);

    /**
     * @return Money
     */
    public function getPrice();

    /**
     * @return Money
     */
    public function getSum();

    /**
     * @return CartImageInterface[]
     */
    public function getImages();

    /**
     * @return bool
     */
    public function hasImages();

    /**
     * @return null|CartImageInterface
     */
    public function getFirstImage();

    /**
     * @return array
     */
    public function getAttributes();

    /**
     * getter for a single attributes
     *
     * @param  string $attribute
     *
     * @return mixed|null
     */
    public function getAttribute($attribute);
}