<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 12.06.17
 * Time: 11:39
 */

namespace Rodacker\Cart\Exception;

class CanNotLoadCartException extends \Exception
{

}