<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 10:39
 */

namespace Rodacker\Cart\Exception;

class CartItemNotFoundException extends \Exception
{

}