<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Identifier;

/**
 * Interface CartIdentifierInterface
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
interface CartIdentifierInterface
{

    /**
     * get an identifier string
     *
     * @return string
     */
    public function get();

    /**
     * generate a new identifier string
     *
     * @return string
     */
    public function generate();

    /**
     * clear the identifier
     *
     * @return null
     */
    public function clear();

}