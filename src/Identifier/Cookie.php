<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Identifier;

use Rodacker\Cart\Utils\Helper;

/**
 * Class Cookie
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class Cookie implements CartIdentifierInterface
{

    /**
     * tries to get the local identifier attribute, if not set, tries to read
     * it from the $_COOKIE array for the key Helper::IDENTIFIER_KEY, generates
     * it if it does not exist
     *
     * @return string
     */
    public function get()
    {
        if (array_key_exists(Helper::IDENTIFIER_KEY, $_COOKIE)) {
            return $_COOKIE[Helper::IDENTIFIER_KEY];
        }

        return $this->generate();
    }

    /**
     * generates a new identifier using the Helper::generateIdentifier method
     * and adds it to the  under the Helper::IDENTIFIER_KEY
     *
     * @return string
     *
     * @see Helper::generateIdentifier()
     */
    public function generate()
    {
        $identifier = Helper::generateIdentifier();

        setcookie(Helper::IDENTIFIER_KEY, $identifier, 0, '/');

        return $identifier;
    }

    /**
     * set the internal id to null and clear the cookie via setcookie()
     *
     * @return null
     */
    public function clear()
    {
        setcookie(Helper::IDENTIFIER_KEY, null, -1, '/');

        return null;
    }

}