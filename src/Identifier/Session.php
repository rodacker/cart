<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Identifier;

use Rodacker\Cart\Utils\Helper;

/**
 * Class SessionIdentifier
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class Session implements CartIdentifierInterface
{

    /**
     * constructor checks if we already have a session otherwise starts it
     *
     * SessionKeyIdentifier constructor.
     */
    public function __construct()
    {
        $status = session_status();
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    /**
     * tries to get the identifier from the $_SESSION array for key
     * Helper::IDENTIFIER_KEY, generates it if it does not exist
     *
     * @return string
     */
    public function get()
    {
        if (array_key_exists(Helper::IDENTIFIER_KEY, $_SESSION)) {
            return $_SESSION[Helper::IDENTIFIER_KEY];
        }

        return $this->generate();
    }

    /**
     * generates a new identifier using the Helper::generateIdentifier method
     * and adds it to the session under the Helper::IDENTIFIER_KEY
     *
     * @return string
     *
     * @see Helper::generateIdentifier()
     */
    public function generate()
    {
        $identifier = Helper::generateIdentifier();

        $_SESSION[Helper::IDENTIFIER_KEY] = $identifier;

        return $identifier;
    }

    /**
     * unset the cart identifier in global $_SESSION array
     *
     * @return null
     */
    public function clear()
    {
        if (array_key_exists(Helper::IDENTIFIER_KEY, $_SESSION)) {
            unset($_SESSION[Helper::IDENTIFIER_KEY]);
        }

        return null;
    }

}