<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 06.06.17
 * Time: 20:24
 */

namespace Rodacker\Cart\Product;

use Money\Money;
use Rodacker\Cart\Image\CartImageInterface;

/**
 * Interface CartItemInterface
 *
 * @package Rodacker\Cart
 */
interface CartProductInterface
{

    /**
     * the id of the product
     *
     * @return int|string
     */
    public function getId();

    /**
     * the name of the item
     *
     * @return mixed
     */
    public function getName();

    /**
     * the price in eurocents (1€ = 100, 10€ = 1000, 100€ = 10000, etc.)
     *
     * @return Money
     */
    public function getPrice();

    /**
     * array if images, can return an empty array if you want to implement images in another way
     *
     * @return CartImageInterface[]
     */
    public function getImages();
}