<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Product;

use Money\Money;
use Rodacker\Cart\Image\CartImageInterface;

/**
 * Class Product
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class Product implements CartProductInterface
{

    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Money
     */
    private $price;

    /**
     * @var CartImageInterface[]
     */
    private $images;

    /**
     * Product constructor.
     *
     * @param string               $name
     * @param int                  $price
     * @param CartImageInterface[] $images
     */
    public function __construct($name, $price, array $images)
    {
        $this->id     = uniqid('product-');
        $this->name   = $name;
        $this->price  = $price;
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int|Money
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

}