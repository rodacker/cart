<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 15.06.17
 * Time: 13:25
 */

namespace Rodacker\Cart\Image;

/**
 * Interface CartImageInterface
 *
 * @package Rodacker\Cart
 */
interface CartImageInterface
{

    /**
     * @return string
     */
    public function getRelativePathname();

    /**
     * @return string
     */
    public function getAbsolutePathname();
}