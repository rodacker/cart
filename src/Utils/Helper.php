<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 12.06.17
 * Time: 11:23
 */

namespace Rodacker\Cart\Utils;

final class Helper
{

    const IDENTIFIER_KEY = 'rodacker_cart_identifier';

    const STORAGE_KEY = 'rodacker_cart_storage';

    /**
     * @return string
     */
    public static function generateIdentifier()
    {
        return md5(uniqid(null, true));
    }
}