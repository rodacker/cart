<?php
/**
 * Created by Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * Date: 06.06.17
 * Time: 20:23
 */

namespace Rodacker\Cart;

use Money\Money;
use Rodacker\Cart\Exception\CartItemNotFoundException;
use Rodacker\Cart\Item\CartItemInterface;

/**
 * Interface CartInterface
 *
 * @package Rodacker\Cart
 */
interface CartInterface
{

    /**
     * @return CartItemInterface[]
     */
    public function getItems();

    /**
     * @param CartItemInterface $item
     */
    public function addItem( CartItemInterface $item );

    /**
     * @param CartItemInterface $item
     */
    public function removeItem( CartItemInterface $item );

    /**
     * @param int $id
     *
     * @return CartItemInterface
     * @throws CartItemNotFoundException
     */
    public function getItem($id);

    /**
     * @return mixed
     */
    public function clear();

    /**
     * @return mixed
     */
    public function getItemCount();

    /**
     * @return bool
     */
    public function isEmpty();

    /**
     * @return Money
     */
    public function getTotal();

    /**
     *  save the cart
     */
    public function save();

    /**
     *  save the cart
     */
    public function restore();

}