<?php

/*
 * This file is part of Hashids.
 *
 * (c) Ivan Akimov <ivan@barreleye.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rodacker\Cart\Test;

use Money\Currency;
use Money\Money;
use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Cart;
use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Exception\CartItemNotFoundException;
use Rodacker\Cart\Identifier\Session;
use Rodacker\Cart\Item\CartItem;
use Rodacker\Cart\Product\CartProductInterface;
use Rodacker\Cart\Storage\SessionStorage;

/**
 * Class CartTest
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * todo: add phpdoc to functions
 * todo: add test covering edge case
 */
class CartTest extends TestCase
{

    /** @var  Session */
    private $identifier;

    /** @var  SessionStorage */
    private $storage;

    /** @var  Cart */
    private $cart;

    /** @var  CartItem */
    private $item;

    protected function setUp()
    {
        parent::setUp();

        $this->identifier = new Session();
        $this->storage    = new SessionStorage();
        $this->cart       = new Cart($this->identifier, $this->storage, new Currency('EUR'));
        $this->item       = new CartItem($this->getProductMock(), Money::EUR(2000));
    }

    public function testInterface()
    {
        $this->assertInstanceOf(CartInterface::class, $this->cart);
    }

    public function testGetItems()
    {
        $empty = $this->cart->getItems();

        $this->assertTrue(is_array($empty));
        $this->assertTrue(count($empty) === 0);
    }

    public function testItemCount()
    {
        $count = $this->cart->getItemCount();
        $this->assertTrue($count === 0);
    }

    public function testUniqueItemCount()
    {
        $count = $this->cart->getItemCount(true);
        $this->assertTrue($count === 0);
    }

    public function testAddItem()
    {

        $this->cart->addItem($this->item);

        $count = $this->cart->getItemCount();
        $this->assertTrue($count === 1);
    }

    public function testGetItemWithoutExistingId()
    {
        $this->cart->clear();
        $this->addItem();
        $this->expectException(CartItemNotFoundException::class);
        $this->cart->getItem('a fake id that does not exist');
    }

    public function testGetItem()
    {
        $this->cart->clear();
        $this->addItem();
        $item = $this->cart->getItem($this->getItemId());

        $this->assertInstanceOf(CartItem::class, $item);
        $this->assertEquals($item, $this->item);
    }

    public function testClear()
    {
        $this->cart->clear();
        $this->addItem();
        $this->assertEquals(1, $this->cart->getItemCount());

        $this->cart->clear();

        $this->assertEquals(0, $this->cart->getItemCount());
    }

    public function testIsEmpty()
    {
        $this->cart->clear();
        $this->assertTrue($this->cart->isEmpty());
        $this->addItem();
        $this->assertFalse($this->cart->isEmpty());
    }

    /**
     * @test
     */
    public function removeItem()
    {
        $this->cart->clear();
        $this->addItem();
        $this->assertFalse($this->cart->isEmpty());

        $this->cart->removeItem($this->item);
        $this->assertEquals(0, $this->cart->getItemCount(), 0);
        $this->assertTrue($this->cart->isEmpty());
    }

    /**
     * @dataProvider providerTestGetTotal
     */
    public function testGetTotal($itemCount, $expectedTotal)
    {
        $this->cart->clear();
        $items = $this->getItems($itemCount);
        foreach ($items as $item) {
            $this->cart->addItem($item);
        }

        $total = $this->cart->getTotal();

        $this->assertEquals($expectedTotal, $this->cart->getTotal()->getAmount());
    }

    public function providerTestGetTotal()
    {
        $items = [
            'two items '  => [2, "2000"],
            'three items' => [3, "3000"],
        ];

        return $items;
    }

    public function testRestore()
    {

        $mock = $this->getMockBuilder(SessionStorage::class)->getMock();
        $mock->expects($this->once())->method('load')->will(
            $this->returnValue($this->cart)
        );

        $cart = new Cart($this->identifier, $mock, new Currency('EUR'));

        $this->assertInstanceOf(CartInterface::class, $cart);
    }

    public function testAddingItemTwice()
    {
        $this->cart->clear();

        $item = $this->getItem(2000, 2);
        $this->cart->addItem($item);
        $this->cart->addItem($item);

        $count = $this->cart->getItemCount();
        $uniqueCount = $this->cart->getItemCount(true);

        $this->assertEquals(1, $uniqueCount);
        $this->assertEquals(4, $count);
    }

    public function testIncreasingItemQuantity()
    {
        $this->cart->clear();

        $item = $this->getItem(2000, 3);
        $this->cart->addItem($item);
        $this->cart->updateItemQuantity($item, 2);

        $count = $this->cart->getItemCount();

        $this->assertEquals(5, $count);
    }

    public function testDecreasingItemQuantity()
    {
        $this->cart->clear();

        $item = $this->getItem(2000, 3);
        $this->cart->addItem($item);
        $this->cart->updateItemQuantity($item, 1, true );

        $count = $this->cart->getItemCount();

        $this->assertEquals(2, $count);
    }

    private function getItem($value = 1000, $quantity = 1)
    {
        return new CartItem($this->getProductMock(),
            Money::EUR($value), $quantity);
    }

    private function getItems($number = 5)
    {
        $items = [];

        $i = 0;
        while ($i < $number) {
            $items[] = $this->getItem();
            $i++;
        }

        return $items;
    }

    /**
     * @return CartProductInterface
     */
    private function getProductMock()
    {

        return $this->getMockBuilder(CartProductInterface::class)
            ->setMockClassName('Product')
            ->getMock();
    }

    private function addItem($item = null)
    {
        if ($item === null) {
            $item = $this->item;
        }
        $this->cart->addItem($item);
    }

    private function getItemId()
    {
        $reflection = new \ReflectionClass($this->item);
        $property   = $reflection->getProperty('id');
        $property->setAccessible(true);

        return $property->getValue($this->item);
    }
}
