<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Test\Utils;

use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Utils\Helper;

/**
 * Class HelperTest
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class HelperTest extends TestCase
{

    /**
     * test identifier generation
     */
    public function testIdentifierGeneration()
    {

        $identifier = Helper::generateIdentifier();
        $this->assertTrue(is_string($identifier));

        $this->assertEquals(1, preg_match('/^[a-f0-9]{32}$/', $identifier));
    }

    /**
     * testing the presence of needed constants
     */
    public function testConstants()
    {

        $helper = new \ReflectionClass(Helper::class);
        $constants = $helper->getConstants();
        $this->assertArrayHasKey('IDENTIFIER_KEY', $constants);
        $this->assertArrayHasKey('STORAGE_KEY', $constants);
    }

}