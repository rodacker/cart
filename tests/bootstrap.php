<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace {

    if ( !($loader = include __DIR__.'/../vendor/autoload.php')) {
        die(
        <<<EOT
You need to install the project dependencies using Composer:
$ wget http://getcomposer.org/composer.phar
OR
$ curl -s https://getcomposer.org/installer | php
$ php composer.phar install --dev
$ phpunit
EOT
        );
    }

    $loader->add('Rodacker\Cart\Tests', __DIR__);
}

namespace Rodacker\Cart\Identifier {


    function session_status()
    {
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if ($step['function'] === 'setUp'
                && ( $step['class'] === "Rodacker\Cart\Test\Identifier\SessionTest" ||
                    $step['class'] === "Rodacker\Cart\Test\CartTest"
                )
            ) {
                return PHP_SESSION_NONE;
            }
        }

        return PHP_SESSION_ACTIVE;
    }

    function session_start()
    {
        if ( !isset($_SESSION)) {
            \session_start();
        }
    }
}

namespace Rodacker\Cart\Storage {

    function session_status()
    {
        $backtrace = debug_backtrace();
        foreach ($backtrace as $step) {
            if ($step['function'] === 'setUp'
                && $step['class']
                == "Rodacker\Cart\Test\Storage\SessionStorageTest"
            ) {
                return PHP_SESSION_NONE;
            }
        }

        return PHP_SESSION_ACTIVE;
    }

    function session_start()
    {
        if ( !isset($_SESSION)) {
            \session_start();
        }
    }
}

namespace Rodacker\Cart {

    function session_start()
    {
        if ( !isset($_SESSION)) {
            \session_start();
        }
    }
}