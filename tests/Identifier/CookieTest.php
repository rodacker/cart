<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Test\Identifier;

use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Identifier\CartIdentifierInterface;
use Rodacker\Cart\Identifier\Cookie;
use Rodacker\Cart\Utils\Helper;

class CookieTest extends TestCase
{

    /** @var  Cookie */
    private $identifier;

    protected function setUp()
    {
        parent::setUp();

        $this->identifier = new Cookie();
    }

    /**
     * @test
     */
    public function implementsInterface()
    {

        $this->assertInstanceOf(
            CartIdentifierInterface::class,
            $this->identifier
        );
    }

    /**
     * @test
     */
    public function generateIdentifier()
    {
        // generate id
        $identifier = $this->identifier->generate();
        $this->assertTrue(is_string($identifier));
    }

    /**
     * @test
     */
    public function getIdentifier()
    {
        // get id
        $identifier = $this->identifier->get();
        $this->assertTrue(is_string($identifier));
    }

    /**
     * @test
     */
    public function generateNewIdentifier()
    {
        // get id
        $existing = $this->identifier->get();

        // generate new id
        $new = $this->identifier->generate();
        $this->assertNotEquals($new, $existing);
    }

    /**
     * @test
     */
    public function clearIdentifier() {

        $this->assertNull($this->identifier->clear());
    }

}