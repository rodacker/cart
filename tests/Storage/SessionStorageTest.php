<?php

/*
 * This file is part of Hashids.
 *
 * (c) Ivan Akimov <ivan@barreleye.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Rodacker\Cart\Test\Storage;

use Money\Currency;
use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Cart;
use Rodacker\Cart\CartInterface;
use Rodacker\Cart\Exception\CanNotLoadCartException;
use Rodacker\Cart\Identifier\Session;
use Rodacker\Cart\Storage\CartStorageInterface;
use Rodacker\Cart\Storage\SessionStorage;
use Rodacker\Cart\Utils\Helper;

class SessionStorageTest extends TestCase
{

    /** @var  Session */
    private $identifier;

    /** @var  SessionStorage */
    private $storage;

    /** @var  Cart */
    private $cart;

    protected function setUp()
    {
        parent::setUp();

        $this->identifier = new Session();
        $this->storage    = new SessionStorage();
        $this->cart       = new Cart($this->identifier, $this->storage, new Currency('EUR'));
    }

    public function testInterface()
    {
        $this->assertInstanceOf(CartStorageInterface::class, $this->storage);
    }

    public function testLoadException()
    {
        unset($_SESSION[Helper::STORAGE_KEY]);
        $this->expectException(CanNotLoadCartException::class);
        $cart = $this->storage->load($this->identifier);
    }

    public function testLoad()
    {
        $this->storage->save($this->cart);
        $cart = $this->storage->load($this->identifier);

        $this->assertInstanceOf(CartInterface::class, $cart);
        $this->assertEquals($cart, $this->cart);
    }

    public function testSave() {
        $cart = $this->storage->save($this->cart);
        $this->assertInstanceOf(CartInterface::class, $cart);
        $this->assertEquals($cart, $this->cart);
    }

    public function testDeleteNotInSession() {
        $cart = $this->storage->delete($this->identifier);
        $this->assertNull($cart);
    }

}
