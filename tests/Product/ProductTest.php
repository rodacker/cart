<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\Cart\Test\Item;

use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Product\Product;
use Rodacker\Cart\Item\CartImageInterface;
use Rodacker\Cart\Product\CartProductInterface;

class ProductTest extends TestCase
{

    /** @var  CartProductInterface */
    private $prodcut;

    protected function setUp()
    {
        parent::setUp();

        $this->prodcut = new Product(
            'my first product',
            990,
            [$this->getImageMock()]
        );
    }

    public function testInterface()
    {
        $this->assertInstanceOf(CartProductInterface::class, $this->prodcut);
    }

    public function testProduct() {

        // name
        $this->assertTrue(is_string($this->prodcut->getName()));
        $this->assertEquals('my first product', $this->prodcut->getName());

        // price
        $this->assertTrue(is_numeric($this->prodcut->getPrice()));
        $this->assertFalse(is_float($this->prodcut->getPrice()));
        $this->assertTrue(is_int($this->prodcut->getPrice()));
        $this->assertEquals(990, $this->prodcut->getPrice());

        // images
        $this->assertTrue(is_array($this->prodcut->getImages()));

    }


    private function getImageMock()
    {
        return $this->getMockBuilder(CartImageInterface::class)
            ->setMockClassName('CartImage')
            ->getMock();
    }

}